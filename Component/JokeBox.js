import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  ImageBackground,
  ScrollView,
} from "react-native";
import Box from "../assets/jokeBox.png";
const JokeBox = (props) => {
  return (
    <ImageBackground
      style={styles.image}
      source={require("../assets/jokeBox.png")}
      resizeMode="contain"
    >
      <View style={styles.jokeCon}>
        <ScrollView>
          <Text style={styles.text}>{props.joke} </Text>
          <View>
  <Text style={styles.text}>{props.jokePart? "lmao" : ''}</Text>
          </View>
        </ScrollView>
      </View>
      {/* <View style={styles.jokeCon}>
         
          <Text style={styles.text}>{props.joke.delivery}</Text>
        </View> */}
    </ImageBackground>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 20,
  },
  jokeCon: {
    padding: "20%",
    paddingVertical: "12%",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: 300,
    height: 200,

    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
  },
});
export default JokeBox;
