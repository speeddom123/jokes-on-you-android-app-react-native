import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  ImageBackground,
} from "react-native";
import Box from "../assets/jokeBox.png";
const JokeTitle = (props) => {
  return (
    <View style={styles.jokeCon}>
      <Text style={styles.text}>{props.title}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  jokeCon: {
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 20,
  },
  image: {
    width: 300,
    height: 200,

    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#6934B3",
    fontSize: 50,
    fontWeight: "bold",
  },
});
export default JokeTitle;
