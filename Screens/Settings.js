import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  ScrollView,
  FlatList,
  Modal,
  TouchableOpacity,
  Picker,
} from "react-native";

const Settings = (props) => {
  const [enteredGoal, setGoal] = useState("");
  const goalInputHandler = (enteredText) => {
    setGoal(enteredText);
  };
  const goalHandler = () => {
    props.addGoal(enteredGoal);
    setGoal("");
  };

  return (
    <Modal
      style={styles.modal}
      visible={props.visible}
      animationType="fade"
      transparent={true}
    >
      <View style={styles.inputContainer}>
        <View style={styles.Title}>
          <Text style={styles.TitleText}>Settings</Text>
        </View>

        <ScrollView style={styles.scroll}>
          <View style={{ flex: 1 }}>
            <Text>Joke Categories:</Text>
            <Picker>
              <Picker.Item label="Any" value="Any" />
              <Picker.Item label="Miscellaneous" value="Miscellaneous" />
              <Picker.Item label="Dark" value="Dark" />
              <Picker.Item label="Pun" value="Pun" />
              <Picker.Item label="Programming" value="Programming" />

            </Picker>
          </View>
          <View style={{ flex: 1 }}>
            <Text>Language:</Text>
            <Picker>
              <Picker.Item label="English" value="" />
              <Picker.Item label="de-German" value="?lang=de" />
            </Picker>
          </View>
    
          <View style={{ flex: 1 }}>
            <Text>BlackList:</Text>
            <Picker>
              <Picker.Item label="NSfw" value="nsfw" />
             
              <Picker.Item label="Political" value="political" />

            </Picker>
          </View>
        </ScrollView>

        <View style={styles.buttonContainer}>
          <View style={styles.button}>
            <Button title="ADD" onPress={goalHandler} />
          </View>
          <View style={styles.button}>
            <Button title="Cancel" color="red" onPress={props.onCancel} />
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    justifyContent: "center",

    backgroundColor: "white",
    borderRadius: 20,

    marginVertical: "50%",
    marginHorizontal: "10%",
  },
  scroll: {
    padding: 20,
  },

  Title: {
    alignItems: "center",
    padding: 10,
  },
  TitleText: {
    fontSize: 20,
  },
  words: {
    flexDirection: "row",

    marginVertical: "2%",
    marginHorizontal: "20%",
  },

  textInput: {
    width: "80%",
    borderBottomColor: "black",
    borderBottomWidth: 1,
    padding: 10,
    marginBottom: 10,
  },
  buttonContainer: {
    marginVertical: 20,
    flexDirection: "row",

    justifyContent: "space-between",
    width: "60%",
    alignSelf: "center",
  },
  button: {
    width: "40%",
  },
});
export default Settings;
