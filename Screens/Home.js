import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";

import { NavigationContainer } from "@react-navigation/native";
import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableOpacity,
  Alert,
  Animated,
  Easing,
} from "react-native";
import Settings from "./Settings";

const Home = ({ navigation }) => {
  const animatedValue = useRef(new Animated.Value(0)).current;
  const [modal, setModal] = useState(false);
  const [YValue, SetY] = useState(new Animated.Value(0));
  const ModalHandler = () => {
    setModal((prev) => {
      !prev;
    });
  };

  const cancelModalHandler = () => {
    setModal(false);
  };
  const Bounce = () => {
  
    Animated.timing(
     YValue,
      {
        toValue: 100,
        duration: 4000,
        useNativeDriver:false,
        easing: Easing.linear
      },
       SetY(new Animated.Value(0))
    ).start()
  }
  const cycleAnimation =() => {
    Animated.sequence([
      Animated.timing(YValue, {
        toValue: 15,
        duration: 800,
        delay: 20,
        useNativeDriver: false
      }),
      Animated.timing(YValue, {
        toValue: 0,
        duration: 800,
        useNativeDriver: false
      })
    ]).start(event => {
      if (event.finished) {
        cycleAnimation();
      }
    });
  }
  useEffect(() => {
    cycleAnimation()
  }, []);

  return (
   
    <View style={styles.container}>
      <View style={styles.Game}>
        <Animated.View
          style={
           [styles.logoCon,{ translateY: YValue }]}
           
           
        >
          <Image
            style={styles.logo}
            source={require("../assets/newlogo.png")}
          />
        </Animated.View>

        <View style={styles.button}>
          <Button
            color="#A101A6"
            title="Zingers"
            onPress={() => navigation.navigate("OneLiners")}
           
          />
        </View>

        <View style={styles.button}>
          <Button
            color="#A101A6"
            title="Two Part Jokes"
            onPress={() => navigation.navigate("TwoLiners")}
          />
        </View>
      </View>
      <Settings onCancel={cancelModalHandler} visible={modal} />
      <View style={styles.SettingsCon}>
        <TouchableOpacity onPress={ModalHandler}>
          <Image
            style={styles.Settings}
            source={require("../assets/settings.png")}
          />
        </TouchableOpacity>
      </View>
      <View></View>
      <StatusBar style="auto" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#FFE700",
  },
  Game: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  logo: {},
  logoCon: {
    
  },
  SettingsCon: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingTop: 100,
    padding: 10,
  },
  Settings: {
    width: 50,
    height: 50,
  },
  button: {
    paddingTop: 20,
    width: "60%",
    borderRadius: 10,
  },
});

export default Home;
