import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";
import axios from "axios";
import { NavigationContainer } from "@react-navigation/native";
import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Button, Image } from "react-native";
import JokeBox from "../Component/JokeBox";
import Title from "../Component/Title";
import { TouchableOpacity } from "react-native-gesture-handler";
import Settings from "./Settings";
const TwoLiners = () => {
  const [modal, setModal] = useState(false);
  const ModalHandler = () => {
    setModal((prev) => {
      !prev;
    });
    
  };
  const cancelModalHandler = () => {
    setModal(false);
  
  };
  const [joke, setJoke] = useState("");
  const [jokeTell, setJokeTell] = useState(false);
  const [nextJoke, setNextJoke] = useState(0);
  const NumJokes = () => {
  
    setNextJoke((prevJoke) => prevJoke + 1)
    
  };
const resetJoke = () =>{
  setJokeTell(prev=>!prev)

}
  useEffect(() => {
    axios
      .get("https://sv443.net/jokeapi/v2/joke/Any?format=json&type=twopart")

      .then((results) => {
        setJoke(results.data);
        setJokeTell(false)
      });
  }, [nextJoke]); // <-- Have to pass in [] here!
let curJoke;
if(jokeTell){
    curJoke = joke.delivery
   
}else{
    curJoke = joke.setup
  
}
  return (
    <View style={styles.container}>
      <View style={styles.Game}>
        <Title title={"Two Liner"} />

        <View style={styles.logoCon}>
          <JokeBox jokePart={jokeTell} joke={curJoke} />
        </View>
        
        <View style={styles.button}>
          <Button color="#A101A6" title="Part" onPress={resetJoke} />
        </View>
        <View style={styles.button}>
          <Button color="#A101A6" title="Jokes On You" onPress={NumJokes} />
        </View>
      </View>
      <Settings onCancel={cancelModalHandler} visible={modal} />
      <View style={styles.SettingsCon}>
        <TouchableOpacity onPress={ModalHandler}>
          <Image
            style={styles.Settings}
            source={require("../assets/settings.png")}
          />
        </TouchableOpacity>
      </View>
      <StatusBar style="auto" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#FFE700",
  },
  Game: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  logo: {
    width: 300,
    height: 200,
    resizeMode: "contain",
  },
  logoCon: {
    flexDirection: "row",
  },
  SettingsCon: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingTop: 100,
    padding: 10,
  },
  Settings: {
    width: 50,
    height: 50,
  },
  button: {
    paddingTop: 20,
    width: "60%",
    borderRadius: 10,
  },
});

export default TwoLiners;
