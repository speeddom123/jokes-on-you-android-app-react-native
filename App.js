import 'react-native-gesture-handler';
import { StatusBar } from "expo-status-bar";
import { NavigationContainer } from '@react-navigation/native';
import React from "react";
import { StyleSheet, Text, View, Button, Image } from "react-native";
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Screens/Home';
import OneLiners from './Screens/Oneliners';
import TwoLiners from './Screens/TwoLiner';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
  setTestDeviceIDAsync,
} from "expo-ads-admob";
const Stack = createStackNavigator();
export default function App() {
  const bannerError=(e)=>{
    alert(e); 
    console.log(e)
 }
  return (
    <NavigationContainer>
      <AdMobBanner
   style={{marginTop:40}}
   bannerSize="fullBanner"
   adUnitID="ca-app-pub-3940256099942544/6300978111"
   testID="EMULATOR"
   onDidFailToReceiveAdWithError={(e)=>bannerError(e)}
 />
    <Stack.Navigator  screenOptions={{
    headerShown: false
  }}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ title: 'Welcome' }}
      />
      <Stack.Screen name="OneLiners" component={OneLiners} />
      <Stack.Screen name="TwoLiners" component={TwoLiners} />
    </Stack.Navigator>
    
    <AdMobBanner
   
          bannerSize="fullBanner"
          adUnitID="ca-app-pub-3940256099942544/6300978111"
          testID="EMULATOR"
          onDidFailToReceiveAdWithError={(e)=>bannerError(e)}
        />
   
    
        
        
  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#FFE700",
  },
  bottomBanner: {
    position: "absolute",
    bottom: 0,
    flex:1
  },
  Game: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  logo:{
    
  },
  logoCon: {
    flexDirection: "row",

    paddingTop: 100,
  },
  SettingsCon: {
    width:"100%",
    flexDirection: "row",
    justifyContent: 'flex-end',
    paddingTop: 100,
    padding: 10
  },
  Settings: {
    width: 50,
    height: 50,
   
    
  },
  button: {
    paddingTop: 20,
    width: "60%",
    borderRadius: 10
  },
});
